﻿using UnityEngine;

namespace UCraft.Extensions
{
    public static class MonoBehaviourExtensions
    {
        public static TComponent AppendClone<TComponent>(this MonoBehaviour self, Object original, string name, Vector3 position,
            Quaternion rotation)
            where TComponent : MonoBehaviour
        {
            // create the copy of the given original
            var clone = MonoBehaviour.Instantiate(original, position, rotation) as GameObject;

            // attach the new object as a child of the invoking mono behaviour and set the name
            clone.transform.parent = self.transform;
            clone.name = string.IsNullOrEmpty(name) ? clone.name : name;

            // return the cloned object's component
            return clone.GetComponent<TComponent>();
        }

        public static TComponent AppendClone<TComponent>(this MonoBehaviour self, Object original, string name, Vector3 position)
            where TComponent : MonoBehaviour
        {
            return self.AppendClone<TComponent>(original, name, position, Quaternion.identity);
        }

        public static TComponent AppendClone<TComponent>(this MonoBehaviour self, Object original, string name)
            where TComponent : MonoBehaviour
        {
            return self.AppendClone<TComponent>(original, name, Vector3.zero, Quaternion.identity);
        }
    }
}