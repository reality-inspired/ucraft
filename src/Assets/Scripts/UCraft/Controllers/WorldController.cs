﻿using UnityEngine;
using System.Collections;
using System;
using UCraft.Extensions;
using UCraft.Models;
using System.Linq;
using System.Collections.Generic;

namespace UCraft.Controllers
{
    public class WorldController : MonoBehaviour
    {
        public GameController Game { get; private set; }

        public ClockController Clock { get; private set; }

        public OutOfBoundsController OutOfBounds { get; private set; }

        public LevelController CurrentLevel { get; private set; }

        public GameObject levelPrefab;

        public bool destroyLevel;

        public bool HasLevel { get { return CurrentLevel != null; } }

        private void Awake()
        {
            // obtain references to required components
            Game = GetComponentInParent<GameController>();
            Clock = GetComponentInChildren<ClockController>();
            OutOfBounds = GetComponentInChildren<OutOfBoundsController>();
        }

        private void OnValidate()
        {
            if (this.destroyLevel)
            {
                DestroyLevel();
                this.destroyLevel = false;
            }
        }

        public void DestroyLevel()
        {
            // ensure there is a current level
            if (HasLevel)
            {
                // destroy the level and null the property
                Destroy(CurrentLevel.gameObject);
                CurrentLevel = null;
            }
        }

        public void LoadLevel(LevelSettings settings)
        {
            // destroy current level
            DestroyLevel();

            // set the time based on the level settings
            Clock.Set(settings.Hour, settings.Minute, settings.Seconds, settings.SecondsPerMinute);

            // append the new level to the world and generate using the given level settings
            CurrentLevel = this.AppendClone<LevelController>(this.levelPrefab, "Level");
            CurrentLevel.Load(settings);
        }
    }
}