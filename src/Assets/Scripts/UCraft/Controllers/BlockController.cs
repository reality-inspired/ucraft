﻿using UnityEngine;
using System.Collections;

namespace UCraft.Controllers
{
    public class BlockController : MonoBehaviour
    {
        public BoxCollider Bounds { get; private set; }

        public ChunkController Chunk { get; private set; }

        public int X { get; private set; }

        public int Y { get; private set; }

        public int Z { get; private set; }

        public GameController Game { get { return GameController.Current; } }

        public WorldController World { get { return Game.World; } }

        public LevelController Level { get { return World.CurrentLevel; } }

        public BlockController Up { get { return Chunk[X, Y + 1, Z]; } }

        public BlockController Down { get { return Chunk[X, Y - 1, Z]; } }

        public BlockController Left { get { return Chunk[X - 1, Y, Z]; } }

        public BlockController Right { get { return Chunk[X + 1, Y, Z]; } }

        public BlockController Forward { get { return Chunk[X, Y, Z + 1]; } }

        public BlockController Back { get { return Chunk[X, Y, Z - 1]; } }

        private void Awake()
        {
            // obtain references to required components
            Bounds = GetComponent<BoxCollider>();
        }

        public void Init(ChunkController chunk, int x, int y, int z)
        {
            // set the parent chunks
            Chunk = chunk;

            // adjust the collider size that represents the chunk
            Bounds.size = new Vector3(Level.BlockSize, Level.BlockSize, Level.BlockSize);

            // set position properties
            X = x;
            Y = y;
            Z = z;
        }
    }
}