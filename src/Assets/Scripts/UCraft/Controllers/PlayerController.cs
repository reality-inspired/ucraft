﻿using UnityEngine;
using System.Collections;
using System;

namespace UCraft.Controllers
{
    public class PlayerController : MonoBehaviour
    {
        public CharacterController Controller { get; private set; }

        public float speed = 5f;

        public float turn = 90f;

        public float jump = 8f;

        public GameController Game { get { return GameController.Current; } }

        public WorldController World { get { return Game.World; } }

        public LevelController Level { get { return World.CurrentLevel; } }

        private bool _isFalling;

        private Vector3 _gravity;

        private void Awake()
        {
            // obtain references to required components
            Controller = GetComponent<CharacterController>();
        }

        private void Start()
        {
            Controller.Move(Vector3.zero);
        }

        private void FixedUpdate()
        {
            // apply gravity to the player's movement
            _gravity.y -= Level.gravity * Time.fixedDeltaTime;
            Controller.Move(_gravity * Time.fixedDeltaTime);

            if (Controller.isGrounded)
            {
                if (_isFalling)
                {
                    // character hit the ground, determine damage
                }

                _isFalling = false;
                _gravity.y = 0f;

                // check if player initiated a jump
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    // apply jump force to gravity vector
                    _gravity.y += this.jump;
                }
            }
            else
            {
                _isFalling = true;
            }
        }

        private void Update()
        {
            // rotate transform based on player input
            transform.Rotate(0, Input.GetAxis("Horizontal") * this.turn * Time.deltaTime, 0);

            // apply simple forward/backward movement to the character controller
            Controller.Move(transform.forward * Input.GetAxis("Vertical") * this.speed * Time.deltaTime);
        }

        private void OnDestroy()
        {
        }
    }
}