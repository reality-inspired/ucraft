﻿using UnityEngine;
using System.Collections;
using UCraft.UI;

namespace UCraft.Controllers
{
    public class UIController : MonoBehaviour
    {
        public GameController Game { get; private set; }

        public MainMenu MainMenu { get; private set; }

        public WorldController World { get { return Game.World; } }

        private void Awake()
        {
            // obtain references to required components
            Game = GetComponentInParent<GameController>();
            MainMenu = GetComponentInChildren<MainMenu>();
        }

        public void DisableAll()
        {
            // disable all menus
            MainMenu.gameObject.SetActive(false);
        }

        public void ShowMainMenu()
        {
            // stop time and disable the world
            Time.timeScale = 0f;
            World.gameObject.SetActive(false);

            // disable all menus then activate the main menu
            DisableAll();
            MainMenu.gameObject.SetActive(true);
        }

        public void ReturnToGame()
        {
            // disable all menus
            DisableAll();

            // start time and enable the world
            Time.timeScale = 1.0f;
            World.gameObject.SetActive(true);
        }
    }
}