﻿using UnityEngine;
using System.Collections;
using UCraft.Extensions;
using System.Collections.Generic;

namespace UCraft.Controllers
{
    public class ChunkController : MonoBehaviour
    {
        public BoxCollider Bounds { get; private set; }

        public int X { get; private set; }

        public int Y { get; private set; }

        public int Z { get; private set; }

        public IDictionary<int, IDictionary<int, IDictionary<int, BlockController>>> CurrentBlocks { get; private set; }

        public GameObject emptyBlockPrefab;

        public GameController Game { get { return GameController.Current; } }
        
        public WorldController World { get { return Game.World; } }

        public LevelController Level { get { return World.CurrentLevel; } }

        public ChunkController Up { get { return Level[X, Y + 1, Z]; } }

        public ChunkController Down { get { return Level[X, Y - 1, Z]; } }

        public ChunkController Left { get { return Level[X - 1, Y, Z]; } }

        public ChunkController Right { get { return Level[X + 1, Y, Z]; } }

        public ChunkController Forward { get { return Level[X, Y, Z + 1]; } }

        public ChunkController Back { get { return Level[X, Y, Z - 1]; } }

        public bool HasBlocks { get { return CurrentBlocks != null; } }

        public BlockController this[int x, int y, int z]
        {
            get
            {
                return CurrentBlocks[x][y][z];
            }
        }

        private void Awake()
        {
            // obtain references to required components
            Bounds = GetComponent<BoxCollider>();
        }

        private void OnVisible()
        {
            Debug.Log(gameObject.name + " has been seen!");
        }

        public void Init(int x, int y, int z)
        {
            // set position properties
            X = x;
            Y = y;
            Z = z;

            // adjust the collider size that represents the chunk
            Bounds.size = new Vector3(Level.ChunkWidth, Level.ChunkHeight, Level.ChunkDepth);

            // init blocks
            InitBlocks();
        }

        public void DestroyBlocks()
        {
            if (HasBlocks)
            {
                // destroy all the blocks
                foreach (var x in CurrentBlocks.Values)
                {
                    foreach (var y in x.Values)
                    {
                        foreach (var controller in y.Values)
                        {
                            Destroy(controller.gameObject);
                        }

                        y.Clear();
                    }

                    x.Clear();
                }

                CurrentBlocks.Clear();
                CurrentBlocks = null;
            }
        }

        public void InitBlock(int x, int y, int z)
        {
            // create the block controller entry the current z-axis
            var block = this.AppendClone<BlockController>(emptyBlockPrefab, string.Format("b-{0}-{1}-{2}", x, y, z));
            block.transform.localPosition = GetBlockPosition(x, y, z);
            block.Init(this, x, y, z);

            // attach the block at the current position in the chunk
            CurrentBlocks[x][y][z] = block;
        }

        public void InitBlock(int x, int y)
        {
            // create the dictionary at the current y-axis
            CurrentBlocks[x].Add(y, new Dictionary<int, BlockController>());

            // loop through each block along the z-axis
            for (var z = 0; z < Level.ZBlocks; z++)
            {
                // initialize the z-axis block
                InitBlock(x, y, z);
            }
        }

        public void InitBlock(int x)
        {
            // create the dictionary at the current x-axis
            CurrentBlocks[x] = new Dictionary<int, IDictionary<int, BlockController>>();

            // loop through each block along the y-axis
            for (var y = 0; y < Level.YBlocks; y++)
            {
                // initialize the y-axis block
                InitBlock(x, y);
            }
        }

        public void InitBlocks()
        {
            // destroy existing blocks
            DestroyBlocks();

            // create the blocks matrix
            CurrentBlocks = new Dictionary<int, IDictionary<int, IDictionary<int, BlockController>>>();

            // loop through each block along the x-axis
            for (var x = 0; x < Level.XBlocks; x++)
            {
                // init the x-xis block
                InitBlock(x);
            }
        }

        public Vector3 GetBlockPosition(int x, int y, int z)
        {
            return new Vector3(
                Level.ChunkXStart + x * Level.BlockSize,
                Level.ChunkYStart + y * Level.BlockSize,
                Level.ChunkZStart + z * Level.BlockSize
            );
        }
    }
}