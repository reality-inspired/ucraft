﻿using UnityEngine;
using System.Collections;

namespace UCraft.Controllers
{
    public class CameraController : MonoBehaviour
    {
        protected Camera Camera { get; private set; }
        
        private void Awake()
        {
            // obtain references to required components
            Camera = GetComponent<Camera>();
        }

        public void AttachTo(GameObject gameObject)
        {
            transform.parent = gameObject.transform;
        }
    }
}