﻿using UnityEngine;
using System.Collections;
using UCraft.Models;
using UCraft.Extensions;
using System;
using System.Collections.Generic;

namespace UCraft.Controllers
{
    public class LevelController : MonoBehaviour
    {
        /// <summary>
        /// Represents the width, height, and depth of a single block.
        /// </summary>
        public float BlockSize { get; private set; }

        /// <summary>
        /// Number of chunks along the x-axis of the level.
        /// </summary>
        public int XChunks { get; private set; }

        /// <summary>
        /// Number of chunks along the y-axis of the level.
        /// </summary>
        public int YChunks { get; private set; }

        /// <summary>
        /// Number of chunks along the z-axis of the level.
        /// </summary>
        public int ZChunks { get; private set; }

        /// <summary>
        /// Number of blocks along the x-axis of a chunk within the level.
        /// </summary>
        public int XBlocks { get; private set; }

        /// <summary>
        /// Number of blocks along the y-axis of a chunk within the level.
        /// </summary>
        public int YBlocks { get; private set; }

        /// <summary>
        /// Number of blocks along the z-axis of a chunk within the level.
        /// </summary>
        public int ZBlocks { get; private set; }
        
        public float ChunkWidth { get; private set; }

        public float ChunkHeight { get; private set; }

        public float ChunkDepth { get; private set; }

        public float LevelWidth { get; private set; }

        public float LevelHeight { get; private set; }

        public float LevelDepth { get; private set; }

        public float LevelXStart { get; private set; }

        public float LevelYStart { get; private set; }

        public float LevelZStart { get; private set; }

        public float ChunkXStart { get; private set; }

        public float ChunkYStart { get; private set; }

        public float ChunkZStart { get; private set; }

        public IDictionary<int, IDictionary<int, IDictionary<int, ChunkController>>> CurrentChunks { get; private set; }

        public PlayerController CurrentPlayer { get; private set; }

        public GameObject chunkPrefab;

        public GameObject playerPrefab;

        public float gravity = 9.8f;

        public GameController Game { get { return GameController.Current; } }

        public WorldController World { get { return Game.World; } }

        public bool HasChunks { get { return CurrentChunks != null; } }

        public bool HasPlayer { get { return CurrentPlayer != null; } }

        public ChunkController this[int x, int y, int z]
        {
            get
            {
                return CurrentChunks[x][y][z];
            }
        }

        private void OnDestroy()
        {
            // destroy all chunks and the player
            DestroyChunks();
            DestroyPlayer();
        }

        public void Load(LevelSettings settings)
        {
            //TODO: add block size to level settings
            BlockSize = 1f;

            // set level dimensions via chunk and block counts
            XChunks = settings.XChunks;
            YChunks = settings.YChunks;
            ZChunks = settings.ZChunks;
            XBlocks = settings.XBlocks;
            YBlocks = settings.YBlocks;
            ZBlocks = settings.ZBlocks;

            // calculate level and chunk sizes
            ChunkWidth = XBlocks * BlockSize;
            ChunkHeight = YBlocks * BlockSize;
            ChunkDepth = ZBlocks * BlockSize;
            LevelWidth = XChunks * ChunkWidth;
            LevelHeight = YChunks * ChunkHeight;
            LevelDepth = ZChunks * ChunkDepth;
            LevelXStart = -LevelWidth / 2f + ChunkWidth / 2f;
            LevelYStart = -LevelHeight / 2f + ChunkHeight / 2f;
            LevelZStart = -LevelDepth / 2f + ChunkDepth / 2f;
            ChunkXStart = -ChunkWidth / 2f + BlockSize / 2f;
            ChunkYStart = -ChunkHeight / 2f + BlockSize / 2f;
            ChunkZStart = -ChunkDepth / 2f + BlockSize / 2f;

            // initialize the level chunks
            InitChunks();

            // set the bounds of the world to accomodate the new level
            World.OutOfBounds.Set(LevelWidth, LevelHeight, LevelDepth);

            // spawn the player after the chunks are loaded
            SpawnPlayer(settings.SpawnPoint);
        }

        public void DestroyChunks()
        {
            if (HasChunks)
            {
                // destroy all the chunks
                foreach (var x in CurrentChunks.Values)
                {
                    foreach (var y in x.Values)
                    {
                        foreach (var controller in y.Values)
                        {
                            Destroy(controller.gameObject);
                        }

                        y.Clear();
                    }

                    x.Clear();
                }

                CurrentChunks.Clear();
                CurrentChunks = null;
            }
        }

        public void InitChunk(int x, int y, int z)
        {
            // create the chunk controller entry the current z-axis
            var chunk = this.AppendClone<ChunkController>(chunkPrefab, string.Format("c-{0}-{1}-{2}", x, y, z));
            chunk.transform.localPosition = GetChunkPosition(x, y, z);
            chunk.Init(x, y, z);

            // attach the chunk at the current position in the level
            CurrentChunks[x][y][z] = chunk;
        }

        public void InitChunk(int x, int y)
        {
            // create the dictionary at the current y-axis
            CurrentChunks[x].Add(y, new Dictionary<int, ChunkController>());

            // loop through each chunk along the z-axis
            for (var z = 0; z < ZChunks; z++)
            {
                // initialize the z-axis chunk
                InitChunk(x, y, z);
            }
        }

        public void InitChunk(int x)
        {
            // create the dictionary at the current x-axis
            CurrentChunks[x] = new Dictionary<int, IDictionary<int, ChunkController>>();

            // loop through each chunk along the y-axis
            for (var y = 0; y < YChunks; y++)
            {
                // initialize the y-axis chunk
                InitChunk(x, y);
            }
        }

        public void InitChunks()
        {
            // destroy existing chunks
            DestroyChunks();

            // create the chunks matrix
            CurrentChunks = new Dictionary<int, IDictionary<int, IDictionary<int, ChunkController>>>();

            // loop through each chunk along the x-axis
            for (var x = 0; x < XChunks; x++)
            {
                // initialize the x-axis chunk
                InitChunk(x);
            }
        }

        public Vector3 GetChunkPosition(int x, int y, int z)
        {
            return new Vector3(
                LevelXStart + x * ChunkWidth,
                LevelYStart + y * ChunkHeight,
                LevelZStart + z * ChunkDepth
            );
        }

        public void DestroyPlayer()
        {
            // ensure there is a current player
            if (HasPlayer)
            {
                // attach the camera to the game
                Game.Camera.AttachTo(Game.gameObject);

                // destroy the player and null the property
                Destroy(CurrentPlayer.gameObject);
                CurrentPlayer = null;
            }
        }

        public void SpawnPlayer(Vector3 position)
        {
            // destroy current player
            DestroyPlayer();

            // append a new player using the set prefab and attach the camera to the player
            CurrentPlayer = this.AppendClone<PlayerController>(playerPrefab, "Player", position);
            Game.Camera.AttachTo(CurrentPlayer.gameObject);
        }
    }
}