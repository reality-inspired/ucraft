﻿using UnityEngine;
using System.Collections;
using System;

namespace UCraft.Controllers
{
    public class OutOfBoundsController : MonoBehaviour
    {
        public BoxCollider Trigger { get; private set; }

        public BoxCollider Up { get; private set; }

        public BoxCollider Down { get; private set; }

        public BoxCollider Left { get; private set; }

        public BoxCollider Right { get; private set; }

        public BoxCollider Forward { get; private set; }

        public BoxCollider Back { get; private set; }

        private void Awake()
        {
            // add the out-of-bounds trigger collider
            Trigger = gameObject.AddComponent<BoxCollider>();
            Trigger.isTrigger = true;

            // add the required box colliders to keep rigid bodies inside of the world
            Up = gameObject.AddComponent<BoxCollider>();
            Down = gameObject.AddComponent<BoxCollider>();
            Left = gameObject.AddComponent<BoxCollider>();
            Right = gameObject.AddComponent<BoxCollider>();
            Forward = gameObject.AddComponent<BoxCollider>();
            Back = gameObject.AddComponent<BoxCollider>();
        }

        private void OnTriggerExit(Collider other)
        {
            // destroy the game object attached to the collider
            Destroy(other.gameObject);
        }

        public void Set(float width, float height, float depth)
        {
            // set the trigger collider size
            Trigger.size = new Vector3(width, height, depth);

            // calculate the halfway points to center the colliders
            var halfWidth = width / 2f;
            var halfHeight = height / 2f;
            var halfDepth = depth / 2f;

            // set the up and down colliders
            Up.size = new Vector3(width, 0f, depth);
            Up.center = new Vector3(0f, halfHeight, 0f);
            Down.size = new Vector3(width, 0f, depth);
            Down.center = new Vector3(0f, -halfHeight, 0f);

            // set the left and right colliders
            Left.size = new Vector3(width, height, 0f);
            Left.center = new Vector3(0f, 0f, halfDepth);
            Right.size = new Vector3(width, height, 0f);
            Right.center = new Vector3(0f, 0f, -halfDepth);

            // set the forward and back colliders
            Forward.size = new Vector3(0f, height, depth);
            Forward.center = new Vector3(halfWidth, 0f, 0f);
            Back.size = new Vector3(0f, height, depth);
            Back.center = new Vector3(-halfWidth, 0f, 0f);
        }
    }
}