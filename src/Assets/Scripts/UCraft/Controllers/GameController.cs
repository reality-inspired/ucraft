﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UCraft.Models;
using System.Linq;
using System;

namespace UCraft.Controllers
{
    public class GameController : MonoBehaviour
    {
        public static GameController Current { get; private set; }

        public CameraController Camera { get; private set; }

        public UIController UI { get; private set; }

        public WorldController World { get; private set; }

        public List<BlockSet> Blocks;

        private void Awake()
        {
            // obtain references to required components
            Camera = GetComponentInChildren<CameraController>();
            UI = GetComponentInChildren<UIController>();
            World = GetComponentInChildren<WorldController>();

            // assign the static game controller to be used everywhere
            Current = this;
        }

        private void Start()
        {
            // the game starts at the main menu
            UI.ShowMainMenu();
        }
    }
}