﻿using UnityEngine;
using System.Collections;
using System;

namespace UCraft.Controllers
{
    public class ClockController : MonoBehaviour
    {
        public const float DegreesPerHour = 360f / 24f;

        public const float DegreesPerMinute = DegreesPerHour / 60f;

        public int hour = 8;

        public int minute = 0;

        public float seconds = 0f;

        public float secondsPerMinute = 1f;

        public bool IsDay { get { return this.hour >= 7 && this.hour <= 19; } }

        public bool IsNight { get { return !IsDay; } }

        private float _degreesPerSecond;

        private float _hourDegrees;

        private float _minuteDegrees;

        private float _degrees;
        
        private void OnValidate()
        {
            // set the time to the new current time
            Apply();
        }

        private void Update()
        {
            // increment seconds based on change in time
            this.seconds += Time.deltaTime;

            // check if we should move to the next minute
            while (this.seconds >= this.secondsPerMinute)
            {
                MoveToNextMinute();
                this.seconds -= this.secondsPerMinute;
            }

            // rotate the skybox based on current hour and tick
            _degrees = _hourDegrees + _minuteDegrees + this.seconds * _degreesPerSecond;
            transform.localRotation = Quaternion.Euler(_degrees, 0f, 0f);
        }

        public void MoveToNextHour()
        {
            // increment the hour
            this.hour++;

            // check if a new day has started
            if (this.hour == 24)
            {
                // TODO: add event or something for day changing?
                this.hour = 0;
            }

            // update performance field
            _hourDegrees = this.hour * DegreesPerHour;
        }

        public void MoveToNextMinute()
        {
            // increment the minute
            this.minute++;

            // check if the hour should increment
            if (this.minute == 60)
            {
                // move to next hour then zero the minute
                MoveToNextHour();
                this.minute = 0;
            }

            // update performance field
            _minuteDegrees = this.minute * DegreesPerMinute;
        }

        public void SetHour(int hour)
        {
            // constrain hour between 0 and 23
            this.hour = hour % 23;

            // update performance field
            _hourDegrees = this.hour * DegreesPerHour;
        }

        public void SetMinute(int minute)
        {
            // constrain minute between 0 and 59
            this.minute = minute % 59;

            // update performance field
            _minuteDegrees = this.minute * DegreesPerMinute;
        }

        public void SetSeconds(float seconds)
        {
            // constraint seconds between 0 and secondsPerMinute
            this.seconds = seconds % this.secondsPerMinute;
        }

        public void SetSecondsPerMinute(float secondsPerMinute)
        {
            // constrain seconds per minute between 1 and 60
            this.secondsPerMinute = Math.Max(1f, secondsPerMinute);
            this.secondsPerMinute = Math.Min(60f, this.secondsPerMinute);

            // update performance field
            _degreesPerSecond = DegreesPerMinute / this.secondsPerMinute;
        }

        public void Set(int hour, int minute, float seconds, float secondsPerMinute)
        {
            // set all the properties using the individual methods
            SetHour(hour);
            SetMinute(minute);
            SetSecondsPerMinute(secondsPerMinute);
            SetSeconds(seconds);
        }

        public void Apply()
        {
            Set(this.hour, this.minute, this.seconds, this.secondsPerMinute);
        }
    }
}