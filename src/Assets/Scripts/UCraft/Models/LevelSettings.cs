﻿using UnityEngine;
using System.Collections;
using System;

namespace UCraft.Models
{
    public class LevelSettings
    {
        public int Hour { get; private set; }

        public int Minute { get; private set; }

        public float Seconds { get; private set; }

        public float SecondsPerMinute { get; private set; }

        /// <summary>
        /// Position indicating where to spawn the player when the level is loaded.
        /// </summary>
        public Vector3 SpawnPoint { get; private set; }

        /// <summary>
        /// Number of chunks along the x-axis of the level.
        /// </summary>
        public int XChunks { get; private set; }

        /// <summary>
        /// Number of chunks along the y-axis of the level.
        /// </summary>
        public int YChunks { get; private set; }

        /// <summary>
        /// Number of chunks along the z-axis of the level.
        /// </summary>
        public int ZChunks { get; private set; }

        /// <summary>
        /// Number of blocks along the x-axis of a chunk within the level.
        /// </summary>
        public int XBlocks { get; private set; }

        /// <summary>
        /// Number of blocks along the y-axis of a chunk within the level.
        /// </summary>
        public int YBlocks { get; private set; }

        /// <summary>
        /// Number of blocks along the z-axis of a chunk within the level.
        /// </summary>
        public int ZBlocks { get; private set; }

        /// <summary>
        /// Creates a set of level settings using the given level chunk and chunk block counts.
        /// </summary>
        /// <param name="spawnPoint">Position to use as player spawn point when the level is loaded</param>
        /// <param name="xChunks">Number of chunks along the x-axis of the level</param>
        /// <param name="yChunks">Number of chunks along the y-axis of the level</param>
        /// <param name="zChunks">Number of chunks along the z-axis of the level</param>
        /// <param name="xBlocks">Number of blocks along the x-axis of a chunk within the level</param>
        /// <param name="yBlocks">Number of blocks along the y-axis of a chunk within the level</param>
        /// <param name="zBlocks">Number of blocks along the z-axis of a chunk within the level</param>
        public LevelSettings(Vector3 spawnPoint, int xChunks, int yChunks, int zChunks, int xBlocks, int yBlocks, int zBlocks)
        {
            if (xChunks <= 0)
            {
                throw new ArgumentOutOfRangeException("xChunks", "Must be greater than zero.");
            }
            if (yChunks <= 0)
            {
                throw new ArgumentOutOfRangeException("yChunks", "Must be greater than zero.");
            }
            if (zChunks <= 0)
            {
                throw new ArgumentOutOfRangeException("zChunks", "Must be greater than zero.");
            }
            if (xBlocks <= 0)
            {
                throw new ArgumentOutOfRangeException("xBlocks", "Must be greater than zero.");
            }
            if (yBlocks <= 0)
            {
                throw new ArgumentOutOfRangeException("yBlocks", "Must be greater than zero.");
            }
            if (zBlocks <= 0)
            {
                throw new ArgumentOutOfRangeException("zBlocks", "Must be greater than zero.");
            }

            // set spawn point
            SpawnPoint = spawnPoint;

            // assign chunk counts
            XChunks = xChunks;
            YChunks = yChunks;
            ZChunks = zChunks;

            // assign chunk block counts
            XBlocks = xBlocks;
            YBlocks = yBlocks;
            ZBlocks = zBlocks;
        }

        /// <summary>
        /// Creates a set of level settings using the given value for all level chunk counts and chunk block counts.
        /// </summary>
        /// <param name="spawnPoint">Position to use as player spawn point when the level is loaded</param>
        /// <param name="chunks">Number of chunks along the x, y, and z axes of the level</param>
        /// <param name="blocks">Number of blocks along the x, y, and z axes of a chunk within the level</param>
        public LevelSettings(Vector3 spawnPoint, int chunks, int blocks)
            : this(spawnPoint, chunks, chunks, chunks, blocks, blocks, blocks)
        {
        }

        public void SetTime(int hour, int minute, float seconds, float secondsPerMinute)
        {
            Hour = hour;
            Minute = minute;
            Seconds = seconds;
            SecondsPerMinute = secondsPerMinute;
        }
    }
}