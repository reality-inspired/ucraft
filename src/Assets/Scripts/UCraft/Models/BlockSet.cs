﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UCraft.Controllers;

namespace UCraft.Models
{
    [Serializable]
    public class BlockSet
    {
        public string name;
        
        public List<BlockController> prefabs;
    }
}