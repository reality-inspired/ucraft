﻿using UnityEngine;
using System.Collections;
using UCraft.Controllers;
using UCraft.Models;

namespace UCraft.UI
{
    public class MainMenu : MonoBehaviour
    {
        public UIController UI { get; private set; }
        
        public WorldController World { get { return UI.World; } }
        
        private void Awake()
        {
            // obtain references to required components
            UI = GetComponentInParent<UIController>();
        }

        private void OnGUI()
        {
            GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));

            GUILayout.BeginArea(new Rect(Screen.width / 2f - 100f, Screen.height / 2f - 100f, 200f, 200f));
            if (GUILayout.Button("New Level"))
            {
                // create new level settings and load them
                var settings = new LevelSettings(Vector3.zero, 5, 5);
                settings.SetTime(8, 0, 0, 1);
                World.LoadLevel(settings);

                // return to the game
                UI.ReturnToGame();
            }
            if (GUILayout.Button("Load Level"))
            {
            }
            GUILayout.EndArea();

            GUILayout.EndArea();
        }
    }
}